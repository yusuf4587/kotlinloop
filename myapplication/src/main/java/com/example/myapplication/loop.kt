package com.example.myapplication

import android.util.Log

fun main() {
    var rows = 11
    println("loop1")
    loop1(rows)
    println("loop2")
    loop2(rows)
    println("loop3")
    loop3(rows)
    println("loop4")
    loop4(rows)
    println("loop5")
    loop5(3)
    println("loop6")
    loop6(rows)
    println("loop7")
    loop7(rows)
}

fun loop1(rows: Int) {
    var k = 0
    var min = 2
    for (i in 1..rows) {
        if (i <= rows / 2) {
            for (space in 1..rows - i) {
                print(" ")
            }
            for (u in 1 until i * 2) { // 12345
                if (u > i) {
                    print(u - min)
                    min += 2
                } else {
                    print(u)
                    min = 2
                }
            }
            println()
        } else {
            for (space in 1..i) {
                print(" ")
            }
            for (u in 1 until (rows - i) * 2) {
                if (u > rows - i) {
                    print(u - min)
                    min += 2
                } else {
                    print(u)
                    min = 2
                }
            }
            println()
        }
    }
}

fun loop2(rows: Int) {
    var p = 0
    for (i in 1..rows) {
        if (i <= rows / 2) {
            for (space in 1..rows - i) {
                print(" ")
            }
            var plus = 1
            var min = 2
            for (u in 1 until i * 2) { // i->2  2*2 = 4 until 1-3
                if (u > 1) {
                    if (u > i) {
                        print(i + plus - min)
                        min += 2
                    } else {
                        print(i + plus)
                    }
                    plus++
                } else {
                    print(i)
                }
            }
            println()
        } else {
            for (space in 1..i) {
                print(" ")
            }
            var plus = 1
            var min = 2
            for (u in 1 until (rows - i) * 2) { // i->2  2*2 = 4 until 1-3
                if (u > 1) {
                    if (u > rows - i) {
                        print((rows - i) + plus - min)
                        min += 2
                    } else {
                        print((rows - i) + plus)
                    }
                    plus++
                } else {
                    print((rows - i))
                }
            }
            println()
        }
    }
}

fun loop3(rows: Int){
    for (i in 1 .. rows){
        for (u in 1 .. i){
            print(u)
        }
        println()
    }
}

fun loop4(rows: Int){
    for (i in 1 .. rows){
        for (u in 1 .. rows-i){
            print(u)
        }
        println()
    }
}
fun loop5(rows: Int){
    var point = 1
    for (i in 1 .. rows){
        for (u in 1 .. i){
            print(point)
            point++
        }
        println()
    }
}
fun loop6(rows: Int){
    var min = 2
   for ( i in  1 .. rows){
       for (u in 1 ..rows){
           if(u>rows/2+1) {
               print(u-min)
               min +=2
           }else{
               print(u)
           }
       }
       min=2
       println()
   }
}
fun loop7(rows: Int){
    var min = 2
    var ar:ArrayList<Int> = arrayListOf()
    for ( i in  1 .. rows/2+1){
        for (p in 1 .. i){
            ar.add(p)
        }
        for (u in 1 ..rows){
            if(u>rows/2+1) {
                if(ar.contains(u-min)){
                    print(u-min)
                }else{
                    print("-")
                }
                min +=2
            }else{
                if(ar.contains(u)){
                    print(u)
                }else{
                    print("-")
                }
            }
        }
        min=2
        ar.clear()
        println()
    }
}