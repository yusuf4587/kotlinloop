package com.example.myapplication

fun main() {
    println("loop1")
    loop1()
    println("loop2")
    loop2()
    println("loop3")
    loop3()
    println("loop4")
    loop4()
    println("loop5")
    loop5()
    println("loop6")
    loop6()
    println("loop7")
    loop7()
}

fun loop1() {
    for (i in 1 .. 5) {
        for (o in 1 .. i) {
            print(o)
        }
        println()
    }
}

fun loop2() {
    for (i in 0 .. 5) {
        for (o in 0 .. i) {
            print(i)
        }
        println()
    }
}

fun loop3(){
    for ( i in 0 .. 5){
        for ( i in 0 .. i){
            print("* ")
        }
        println()
    }
}

fun loop4(){
    val row = 5
    var k = 0
    for (i in 1 .. row){
        for ( o in 1 .. row -i){
            print("  ")
        }
        while (k != 2 * i - 1) {
            print("* ")
            k++
        }
        println()
        k=0
    }

}
fun loop5(){
    val row = 5
    var k = 0
    for (i in 1 .. row){
        for ( o in 1 .. row -i){
            print("  ")
        }
        while (k != 2 * i - 1) {
            print("$i ")
            ++k
        }
        println()
        k=0
    }
}
fun loop6(){
    val row = 5
    var k = 0
    for (i in 1 .. row){
        for ( o in 1 .. row -i){
            print("  ")
        }
        while (k != 2 * i - 1) {
            print("$k ")
            ++k
        }
        println()
        k=0
    }
}

fun loop7() {
    val row = 5
    var k = 0
    var count = 0
    var count1 = 0
    var value1 = 0

    for (i in 1..row) {
        for (space in 1..row - i) {
            print("  ")
            ++count // size
        }

        while (k != 2 * i - 1) {//kiri  count dari kiri
            if (count <= row - 1) { // 3<=4 // 4<=4
                print((i + k).toString() + " ")  // 2+0 // 2+1
                ++count
            } else {// kanan
                ++count1
                print((i + k - 2 * count1).toString() + " ")
            }
            ++k
        }
        k = 0
        count = k
        count1 = count

        println()
    }
}


